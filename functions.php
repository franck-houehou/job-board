<?php
date_default_timezone_set('Africa/Porto-Novo');
function load_stylesheets(){
    wp_register_style('plugins', get_template_directory_uri().'/assets/css/plugins.css', array(), 1, 'all');
    wp_enqueue_style('plugins');

    wp_register_style('styles', get_template_directory_uri().'/assets/css/styles.css', array(), 1, 'all');
    wp_enqueue_style('styles');

    wp_register_style('custom', get_template_directory_uri().'/custom.css', array(), 1, 'all');
    wp_enqueue_style('custom');
}

add_action('wp_enqueue_scripts', 'load_stylesheets');


// Load scripts

function addjs(){
    wp_deregister_script('jquery');
    wp_register_script('jquery', get_template_directory_uri().'/assets/js/jquery.min.js', array(), 1, 1, 1);
    wp_enqueue_script('jquery');

    wp_register_script('popper', get_template_directory_uri().'/assets/js/popper.min.js', array(), 1, 1, 1);
    wp_enqueue_script('popper');

    wp_register_script('bootstrap', get_template_directory_uri().'/assets/js/bootstrap.min.js', array(), 1, 1, 1);
    wp_enqueue_script('bootstrap');

    wp_register_script('select2', get_template_directory_uri().'/assets/js/select2.min.js', array(), 1, 1, 1);
    wp_enqueue_script('select2');

    wp_register_script('owl', get_template_directory_uri().'/assets/js/owl.carousel.min.js', array(), 1, 1, 1);
    wp_enqueue_script('owl');

    wp_register_script('ion', get_template_directory_uri().'/assets/js/ion.rangeSlider.min.js', array(), 1, 1, 1);
    wp_enqueue_script('ion');

    wp_register_script('counterup', get_template_directory_uri().'/assets/js/counterup.min.js', array(), 1, 1, 1);
    wp_enqueue_script('counterup');

    wp_register_script('materialize', get_template_directory_uri().'/assets/js/materialize.min.js', array(), 1, 1, 1);
    wp_enqueue_script('materialize');

    wp_register_script('metisMenu', get_template_directory_uri().'/assets/js/metisMenu.min.js', array(), 1, 1, 1);
    wp_enqueue_script('metisMenu');

    wp_register_script('custom', get_template_directory_uri().'/assets/js/custom.js', array(), 1, 1, 1);
    wp_enqueue_script('custom');

    wp_register_script('mycustom', get_template_directory_uri().'/mycustom.js', array(), 1, 1, 1);
    wp_enqueue_script('mycustom');

    wp_localize_script( 'mycustom', 'mycustom_vars', 
        array(
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        )
    );
}

function vb_register_user_scripts() {
  // Enqueue script
  wp_register_script('vb_reg_script', get_template_directory_uri() . '/ajax-registration.js', array('jquery'), null, false);
  wp_enqueue_script('vb_reg_script');
 
  wp_localize_script( 'vb_reg_script', 'vb_reg_vars', array(
        'vb_ajax_url' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => home_url(),
      )
  );
}
add_action('wp_enqueue_scripts', 'vb_register_user_scripts');


/**
 * New User registration
 *
 */
function vb_reg_new_user() {
    try {
        if( !isset( $_POST['nonce'] ) || !wp_verify_nonce( $_POST['nonce'], 'vb_new_user' ) ){
            echo json_encode(array("status" => false, "message" => "Une erreur est survenue lors du traitement de votre requête. Veuillez réessayer plus tard" ));
            die;
        }
        $username = sanitize_text_field($_POST['user']);
        $password = sanitize_text_field($_POST['pass']);
        $email    = sanitize_email($_POST['mail']);
        if( empty($email) || empty($password) ){
            echo json_encode(array("status" => false, "message" => "Veuillez saisir tous les champs" ));
            die;
        }
        $userdata = array(
            'user_login' => $username,
            'user_pass'  => $password,
            'user_email' => $email,
        );
        $user_id = wp_insert_user( $userdata ) ;
        if( !is_wp_error($user_id) ) {
            $headers = array(
                'Content-Type: text/html; charset=UTF-8',
                'From: WordPress Website <admin@example.com>',
            );
            $message = "Félicitations votre compte a été bien créé sur Workoo<br><br>
            Veuillez cliquez sur ".home_url()." pour vous connecter<br><br> Cordialement";
            wp_mail($email, "Workoo | Inscription", $message, $headers);
            echo json_encode(array('status'=>true, 'message'=> 'Inscription réussie... Veuillez vous connecter en appuyant sur <b>Connectez vous</b> juste en bas'));
            die;
        }else {
            echo json_encode(array('status'=>false, 'message'=> $user_id->get_error_message()));
            die;
        } 
    }catch (Exception $e) {
        echo json_encode(array('status'=>false, 'message'=> "Une erreur est survenue lors du traitement de votre requête. Veuillez contacter l'administrateur"));
        die;
    }
}
 
add_action('wp_ajax_register_user', 'vb_reg_new_user');
add_action('wp_ajax_nopriv_register_user', 'vb_reg_new_user');

add_action('wp_ajax_login_user', 'login_user');
add_action('wp_ajax_nopriv_login_user', 'login_user');

function login_user(){
    try {
        if( !isset( $_POST['nonce'] ) || !wp_verify_nonce( $_POST['nonce'], 'vb_user' ) ){
            echo json_encode(array("status" => false, "message" => "Une erreur est survenue lors du traitement de votre requête. Veuillez réessayer plus tard" ));
            die;
        }
        $info = array();
        $info['user_login'] = sanitize_email($_POST['user']);
        $info['user_password'] = sanitize_text_field($_POST['pass']);
        $info['remember'] = false;
        if( empty($info['user_login']) || empty($info['user_password']) ){
            echo json_encode(array("status" => false, "message" => "Veuillez saisir tous les champs" ));
            die;
        }
        $user_signon = wp_signon( $info, false ); // From false to '' since v4.9
        if ( is_wp_error($user_signon) ){
            echo json_encode(array('status'=>false, 'message'=> 'Email ou mot de passe incorrect.'));
            die;
        }else {
            wp_set_current_user($user_signon->ID);
            echo json_encode(array('status'=>true, 'message'=> 'Connexion réussie'));
            die;
        }
    }catch (Exception $e) {
        echo json_encode(array('status'=>false, 'message'=> "Une erreur est survenue lors du traitement de votre requête. Veuillez contacter l'administrateur"));
        die;
    }
}

add_action('wp_ajax_apply_job', 'apply_job');
add_action('wp_ajax_nopriv_apply_job', 'apply_job');

function apply_job(){
    try {

        // var_dump($_POST); die;

        if( !isset( $_POST['apply_nonce'] ) || !wp_verify_nonce( $_POST['apply_nonce'], 'apply' ) || empty($_POST['current_post_id']) ){
            echo json_encode(array("status" => false, "message" => "Une erreur est survenue lors du traitement de votre requête. Veuillez réessayer plus tard" ));
            die;
        }
        
        $last_name = sanitize_text_field($_POST['last_name']);
        $first_name = sanitize_text_field($_POST['first_name']);
        $email = sanitize_email($_POST['email_user']);
        $phone = sanitize_text_field($_POST['phone']);
        $motivation = $_POST['motivation'];
        $current_post_id = sanitize_text_field($_POST['current_post_id']);
        $current_user_id = sanitize_text_field($_POST['current_user_id']);
        $current_user_email = sanitize_text_field($_POST['current_user_email']);
        $current_post_reference = sanitize_text_field($_POST['current_post_reference']);
        $current_post_title = sanitize_text_field($_POST['current_post_title']);
        $cv='';
        if ($current_user_id=='0') {
            $current_user_id='';
        }

        if( empty($last_name) || empty($first_name) || empty($email) || empty($phone) ){
            echo json_encode(array("status" => false, "message" => "Veuillez saisir tous les champs" ));
            die;
        }

        if(isset($_FILES)){
            if(!empty($_FILES["uploadResume"]["name"])){
                $target_dir_array = wp_upload_dir();
                $target_dir = $target_dir_array['path'];
                $target_file = basename($_FILES["uploadResume"]["name"]);
                $extension = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                $target_file= time().'_'.sanitize_title($_FILES["uploadResume"]["name"]);
                $target_file = $target_file.'.'.$extension;
                $destination = $target_dir.'/'.$target_file;
                if(move_uploaded_file($_FILES["uploadResume"]["tmp_name"], $destination)){
                    $cv = $target_file;
                }
            }
        }
        global $wpdb;
        if($wpdb->insert(
            'wp_applyjob',
            array(
                'last_name' =>$last_name , 
                'first_name' =>$first_name , 
                'email' =>$email , 
                'phone' =>$phone , 
                'motivation' =>$motivation , 
                'cv' =>$cv,
                'current_post_id' =>$current_post_id , 
                'current_user_id' =>$current_user_id ,
                'current_user_email' =>$current_user_email , 
                'current_post_reference' =>$current_post_reference , 
                'date_soumission' =>date("Y-m-d H:i:s") ,
            )
        )==false){
            echo json_encode(array("status" => false, "message" => "Une erreur est survenue lors du traitement de votre requête. Veuillez réessayer plus tard" ));
            die;
        }else{
            $headers = array(
                'Content-Type: text/html; charset=UTF-8',
                'From: WordPress Website <admin@example.com>',
            );
            $message = "Votre candidature pour le poste de ".$current_post_title." a bien été reçue. Si votre dossier est sélectionné, nous aurons le plaisir de vous contacter afin de convenir d’un entretien.<br><br> Cordialement";
            wp_mail($email, "Workoo | Postulation", $message, $headers);
            echo json_encode(array("status" => true, "message" => "Soumission faite avec succès" ));
            die;
        }
    }catch (Exception $e) {
        echo json_encode(array('status'=>false, 'message'=> "Une erreur est survenue lors du traitement de votre requête. Veuillez contacter l'administrateur"));
        die;
    }
}



add_action( 'wp_enqueue_scripts', 'addjs' );

function my_admin_scripts() {
    wp_register_script('my_admin_scripts', get_template_directory_uri() . '/my_admin_scripts.js', array('jquery'));
    wp_enqueue_script('my_admin_scripts');
}
add_action('admin_print_scripts', 'my_admin_scripts');


//Post Type fo job

function workoo_init(){
    //handle secteur taxonomy
    register_taxonomy('secteur', 'job', [
        'labels' => [
            'name' => 'Secteurs',
            'singular_name' => 'Secteur',
            'plural_name' => 'Secteurs',
            'search_items' => 'Rechercher des secteurs',
            'all_items' => 'Tous les secteurs',
            'edit_item' => 'Editer le secteur',
            'update_item' => 'Mettre à jour le secteur',
            'add_new_item' => 'Ajouter un nouveau secteur',
            'new_item_name' => 'Ajouter un nouveau secteur',
            'menu_name' => 'Secteurs'
        ],
        'show_in_rest' => true,
        'hierarchical' => true,
        'show_admin_column' => true,
    ]);

    //handle niveau taxonomy
    register_taxonomy('niveau', 'job', [
        'labels' => [
            'name' => "Niveaux d'études",
            'singular_name' => "Niveau d'étude",
            'plural_name' => "Niveaux d'études",
            'search_items' => "Rechercher des niveaux d'études",
            'all_items' => "Tous les niveaux d'études",
            'edit_item' => "Editer le niveau d'étude",
            'update_item' => "Mettre à jour le niveau d'étude",
            'add_new_item' => "Ajouter un nouveau niveau d'étude",
            'new_item_name' => "Ajouter un nouveau niveau d'étude",
            'menu_name' => "Niveaux d'études"
        ],
        'show_admin_column' => true,
        'hierarchical' => true,
    ]);

    $labels = array( 
            'singular_name' =>"Offre d'emploi",
            'add_new' => 'Ajouter',
            'add_new_item' => 'Ajouter une nouvelle offre',
            'edit_item' => 'Editer une nouvelle offre',
            'new_item' => 'Nouvelle offre',
            'view_item' => 'Voir offre',
            'search_items' => "Recherche d'offre d'emploi",
            'not_found' =>  'Aucune offre trouvée',
            'not_found_in_trash' => 'Aucune offre trouvée dans la corbeille'
        );

    register_post_type('job', [
        'label' => "Offres d'emplois",
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'menu_icon'   => 'dashicons-welcome-learn-more',
        'menu_position' => 3,
        'supports' => ['title'],
        'capability_type' => array('job','jobs'),
            'capabilities' => array(
                'publish_posts' => 'publish_jobs',
                'edit_posts' => 'edit_jobs',
                'edit_others_posts' => 'edit_others_jobs',
                'delete_posts' => 'delete_jobs',
                'delete_others_posts' => 'delete_others_jobs',
                'read_private_posts' => 'read_private_jobs',
                'edit_post' => 'edit_job',
                'delete_post' => 'delete_job',
                'read_post' => 'read_job',
            ),
            'map_meta_cap' => true,
    ]);
}


//Post Type fo job

function workoo_submit_job(){

    // add_menu_page('Offres soumises', 'Offres soumises', 'manage_options', 'offres-soumises', 'workoo_jobs_submit', 'dashicons-welcome-learn-more', 4);

    add_submenu_page('edit.php?post_type=job', 'Soumissions','Soumissions', 'manage_options', 'soumissions', 'workoo_jobs_submit');
}

function workoo_jobs_submit(){

    ob_start(); 

    require_once dirname( __FILE__ ).'/views/workoo-table-list-job.php';
    $template = ob_get_contents();
    ob_clean();
    echo $template ;

    

}


function workoo_add_role_caps() {

        // Add the roles you'd like to administer the custom post types
        $roles = array('administrator', 'editor');
        global $current_user;
        $user_roles = $current_user->roles;
        $user_role = array_shift($user_roles);

        // Loop through each role and assign capabilities
        foreach($roles as $the_role) { 

             $role = get_role($the_role);
             $role->add_cap( 'edit_others_jobs' );
             $role->add_cap( 'delete_others_jobs' );
             $role->add_cap( 'read' );
             $role->add_cap( 'read_job');

             

    
    // var_dump($user_role); die;

                 // $role->add_cap( 'read' );
                 // $role->add_cap( 'read_job');
                 $role->add_cap( 'edit_job' );
                 $role->add_cap( 'edit_jobs' );
                 $role->add_cap( 'edit_published_jobs' );
                 $role->add_cap( 'publish_jobs' );
                 $role->add_cap( 'delete_published_jobs' );

                 if($user_role == 'editor') {
                    // $role->add_cap( 'read_private_jobs' ); 
                    // $role->remove_cap( 'publish_jobs' );
                    $role->remove_cap( 'edit_others_jobs' );
                    $role->remove_cap( 'delete_others_jobs' );
                    // $role->add_cap( 'delete_private_jobs' );
                 }

        }
    }
add_action('admin_init','workoo_add_role_caps');

function workoo_add_custom_box(){

    add_meta_box('workoo_offre',"Détails de l'offre", 'workoo_meta_callback', 'job');
    
}

function workoo_meta_callback($post){

    wp_nonce_field(basename(__File__), 'workoo_nonce');
    $workoo_stored_meta = get_post_meta($post->ID);

    ?>
    <div>
        <div style="margin-bottom: 20px">
            <div style="margin-bottom: 10px">
                <label for="workoo_reference">Référence</label>
            </div>
            <div>
                <input type='text' value="<?php if(!empty($workoo_stored_meta['workoo_reference'])) echo esc_attr($workoo_stored_meta['workoo_reference'][0]); ?>"  id="workoo_reference" name="workoo_reference" style="width: 100%" required>
            </div>
        </div>

        <div style="margin-bottom: 20px">
            <div style="margin-bottom: 10px">
                <label for="workoo_lieu_travail">Lieu de travail</label>
            </div>
            <div>
                <input type='text' value="<?php if(!empty($workoo_stored_meta['workoo_lieu_travail'])) echo esc_attr($workoo_stored_meta['workoo_lieu_travail'][0]); ?>"  id="workoo_lieu_travail" name="workoo_lieu_travail" style="width: 100%" required>
            </div>
        </div>

        <div style="margin-bottom: 20px">
            <div style="margin-bottom: 10px">
                <label for="workoo_date_limite">Date limite</label>
            </div>
            <div>
                <input type='date' value="<?php if(!empty($workoo_stored_meta['workoo_date_limite'])) echo esc_attr($workoo_stored_meta['workoo_date_limite'][0]); ?>"  id="workoo_date_limite" name="workoo_date_limite" style="width: 100%" required>
            </div>
        </div>

        <div style="margin-bottom: 20px">
            <div style="margin-bottom: 10px">
                <label for="workoo_mission">Mission</label>
            </div>
            <div>
                <textarea name="workoo_mission" style="width: 100%" required rows="10" cols="30"><?php if(!empty($workoo_stored_meta['workoo_mission'])) echo esc_attr($workoo_stored_meta['workoo_mission'][0]); ?></textarea>
            </div>
        </div>

        <div style="margin-bottom: 20px">
            <div style="margin-bottom: 10px">
                <label for="workoo_tache">Tâches et activités</label>
            </div>
            <div>
                <textarea name="workoo_tache" style="width: 100%" required rows="10" cols="30"><?php if(!empty($workoo_stored_meta['workoo_tache'])) echo esc_attr($workoo_stored_meta['workoo_tache'][0]); ?></textarea>
            </div>
        </div>

        <div style="margin-bottom: 20px">
            <div style="margin-bottom: 10px">
                <label for="workoo_profil">Profil</label>
            </div>
            <div>
                <textarea name="workoo_profil" required style="width: 100%" rows="10" cols="30"><?php if(!empty($workoo_stored_meta['workoo_profil'])) echo esc_attr($workoo_stored_meta['workoo_profil'][0]); ?></textarea>
            </div>
        </div>

        <div style="margin-bottom: 20px">
            <div style="margin-bottom: 10px">
                <label for="workoo_competence">Compétences</label>
            </div>
            <div>
                <textarea name="workoo_competence" required style="width: 100%" rows="10" cols="30"><?php if(!empty($workoo_stored_meta['workoo_competence'])) echo esc_attr($workoo_stored_meta['workoo_competence'][0]); ?></textarea>
            </div>
        </div>

        <div style="margin-bottom: 20px">
            <div style="margin-bottom: 10px">
                <label for="workoo_remuneration">Rémunération</label>
            </div>
            <div>
                <input type='text' value="<?php if(!empty($workoo_stored_meta['workoo_remuneration'])) echo esc_attr($workoo_stored_meta['workoo_remuneration'][0]); ?>"  id="workoo_remuneration" name="workoo_remuneration" style="width: 100%" required>
            </div>
        </div>

        

        <div style="margin-bottom: 20px">
            <div style="margin-bottom: 10px">
                <label for="workoo_nom_entreprise">Nom de l'entreprise</label>
            </div>
            <div>
                <input type='text' value="<?php if(!empty($workoo_stored_meta['workoo_nom_entreprise'])) echo esc_attr($workoo_stored_meta['workoo_nom_entreprise'][0]); ?>"  id="workoo_nom_entreprise" name="workoo_nom_entreprise" style="width: 100%" required>
            </div>
        </div>

        <div style="margin-bottom: 20px">
            <div style="margin-bottom: 10px">
                <label for="workoo_profil">Candidature</label>
            </div>
            <div>
                <textarea name="workoo_candidature" style="width: 100%" rows="10" cols="30" required><?php if(!empty($workoo_stored_meta['workoo_candidature'])) echo esc_attr($workoo_stored_meta['workoo_candidature'][0]); ?></textarea>
            </div>
        </div>
    </div> 
    <?php
}


function workoo_save_offre($post_id){

    
    //check save statuts
    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    $is_valid_nonce = (isset($_POST['workoo_nonce'])) && wp_verify_nonce($_POST['workoo_nonce'], basename(__File__)) ? 'true' : 'false';
    if($is_autosave || $is_revision || !$is_valid_nonce){
        return;
    }

    if(array_key_exists('workoo_reference', $_POST)){
        update_post_meta($post_id, 'workoo_reference', sanitize_text_field($_POST['workoo_reference']));
    }

    if(array_key_exists('workoo_lieu_travail', $_POST)){
        update_post_meta($post_id, 'workoo_lieu_travail', sanitize_text_field($_POST['workoo_lieu_travail']));
    }

    if(array_key_exists('workoo_date_limite', $_POST)){
        update_post_meta($post_id, 'workoo_date_limite', $_POST['workoo_date_limite']);
    }

    if(array_key_exists('workoo_mission', $_POST)){
        update_post_meta($post_id, 'workoo_mission', sanitize_textarea_field($_POST['workoo_mission']));
    }

    if(array_key_exists('workoo_tache', $_POST)){
        update_post_meta($post_id, 'workoo_tache', sanitize_textarea_field($_POST['workoo_tache']));
    }

    if(array_key_exists('workoo_profil', $_POST)){
        update_post_meta($post_id, 'workoo_profil', sanitize_textarea_field($_POST['workoo_profil']));
    }
    if(array_key_exists('workoo_competence', $_POST)){
        update_post_meta($post_id, 'workoo_competence', sanitize_textarea_field($_POST['workoo_competence']));
    }
    if(array_key_exists('workoo_remuneration', $_POST)){
        update_post_meta($post_id, 'workoo_remuneration', sanitize_text_field($_POST['workoo_remuneration']));
    }
    if(array_key_exists('workoo_nom_entreprise', $_POST)){
        update_post_meta($post_id, 'workoo_nom_entreprise', sanitize_text_field($_POST['workoo_nom_entreprise']));
    }
    if(array_key_exists('workoo_candidature', $_POST)){
        update_post_meta($post_id, 'workoo_candidature', sanitize_textarea_field($_POST['workoo_candidature']));
    }
}


add_action('init', 'workoo_init', 0);
add_action('admin_menu', 'workoo_submit_job');
add_action('add_meta_boxes', 'workoo_add_custom_box');
add_action('save_post', 'workoo_save_offre');

add_filter('manage_job_posts_columns', function($columns){

    return [
        'cb' => $columns['cb'],
        'title' => $columns['title'], 
        'reference' => 'Référence',
        'date_limite' => 'Date limite',
        'date' => $columns['date']
    ];
});

add_filter('manage_job_posts_custom_column', function($column, $postId){

    if($column==='reference'){
        echo get_post_meta($postId, 'workoo_reference', true);
    }

    if($column==='date_limite'){
        if(!empty(get_post_meta($postId, 'workoo_date_limite', true))){

            echo date('d/m/Y',strtotime(get_post_meta($postId, 'workoo_date_limite', true)));

        }else{
            echo "";
        }
        
    }
}, 10, 2);


add_action( 'phpmailer_init', 'send_smtp_email' );
function send_smtp_email( $phpmailer ) {
    $phpmailer->isSMTP();
    $phpmailer->Host       = SMTP_HOST;
    $phpmailer->SMTPAuth   = SMTP_AUTH;
    $phpmailer->Port       = SMTP_PORT;
    $phpmailer->SMTPSecure = SMTP_SECURE;
    $phpmailer->Username   = SMTP_USERNAME;
    $phpmailer->Password   = SMTP_PASSWORD;
    $phpmailer->From       = SMTP_FROM;
    $phpmailer->FromName   = SMTP_FROMNAME;
}

