<?php get_header();?>
<?php 

$args = array(
    'post_type' => 'job',
    'meta_query'  => array(
    array(
        'key' => 'workoo_date_limite',
        'value' => date("Y-m-d H:i:s"),
        'compare' => '>',
        'type' => 'DATETIME'
    )
)
);?>

<div class="page-title search-form">
				<div class="container">
					<div class="row m-0 justify-content-center">
						<div class="col-lg-10 col-md-10">
							<form class="search-big-form shadows">
								<div class="row m-0">
									<div class="col-lg-5 col-md-5 col-sm-12 p-0">
										<div class="form-group">
											<i class="ti-search"></i>
											<input type="text" class="form-control l-radius b-0 b-r" placeholder="Job Title or Keywords">
										</div>
									</div>
									
									<div class="col-lg-5 col-md-4 col-sm-12 p-0">
										<div class="form-group">
											<i class="ti-location-pin"></i>
											<input type="text" class="form-control b-0" placeholder="San Francisco, CA">
										</div>
									</div>
									
									<div class="col-lg-2 col-md-3 col-sm-12 p-0">
										<button type="button" class="btn theme-bg r-radius full-width">Find Jobs</button>
									</div>
								</div>
							</form>	
						</div>
					</div>
				</div>
			</div>

<section class="gray-bg">
				<div class="container">
					<div class="row">
						<!-- Item Wrap Start -->
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="row justify-content-center">

<?php $my_query = new WP_Query( $args );

if($my_query->have_posts()): ?>
	<?php while ($my_query->have_posts()) : $my_query->the_post();  

		$nom_entreprise =  get_post_meta(get_the_ID(), 'workoo_nom_entreprise', true);
		$lieu_travail =  get_post_meta(get_the_ID(), 'workoo_lieu_travail', true);
		$date_limite =  get_post_meta(get_the_ID(), 'workoo_date_limite', true);
		$remuneration =  get_post_meta(get_the_ID(), 'workoo_remuneration', true);
		$lieu_travail =  get_post_meta(get_the_ID(), 'workoo_lieu_travail', true);
		?>
		

	<div class="col-lg-4 col-md-6 col-sm-6">
		<div class="_jb_list73">
			<div class="_jb_list73_header">
				<div class="jobs-like bookmark">
					<label class="toggler toggler-danger"><input type="checkbox"><i class="fa fa-bookmark"></i></label>
				</div>
				<div class="_jb_list72_flex">
					
					<div class="_jb_list72_last">
						<h4 class="_jb_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<div class="_times_jb"><?=$nom_entreprise;?>, <?=$lieu_travail;?></div>
					</div>
				</div>
				<div class="_jb_list72_foot">
					<div class="_times_jb text-right"><?=date('d/m/Y',strtotime($date_limite));?></div>
				</div>
			</div>
			<div class="_jb_list73_middle">
				<div class="_jb_list73_middle_flex">
					<h4 class="_jb_title"><a href="<?php the_permalink(); ?>">Rémunération</a></h4>
					<div class="_times_jb"><?=$remuneration;?>/mois</div>
				</div>
				<div class="applieded_list">
					<div class="_jb_types fulltime_lite"><a href="<?php the_permalink(); ?>">Voir détails</a></div>
				</div>
			</div>
		</div>	
	</div>
<?php endwhile ?>
<?php else : ?>
	<p>Aucune offre n'est disponible actuellement</p>
<?php endif; wp_reset_postdata();?>
			</div>
		</div>
	</div>
</section>
<?php get_footer();?>