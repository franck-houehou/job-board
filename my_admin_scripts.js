jQuery(document).ready(function ($) {
    make_dropdown($, "secteur", "Sélectionner un secteur");
    make_dropdown($, "niveau", "Sélectionner un niveau");
});

function make_dropdown($, taxonomy, label) {
    var opts = '<option value="0">'+label+'</option>';
    $(
        "#taxonomy-" + taxonomy + " #" + taxonomy + "checklist label.selectit"
    ).each(function () {
        var v = $(this).find("input:checkbox").val();
        var k = $(this).text().trim();
        var c = $(this).find("input:checkbox").get(0).checked
            ? ' selected="selected" '
            : "";
        opts += '<option value="' + v + '" ' + c + ">" + k + "</option>";
    });
    $(
        '<div id="taxonomy-' +
        taxonomy +
        '-dropdown-holder"><select id="taxonomy-' +
        taxonomy +
        '-dropdown" style="width:100%;">' +
        opts +
        "</select></div>"
    ).insertAfter(".inside #taxonomy-" + taxonomy);

    $("#taxonomy-" + taxonomy + "-dropdown").change(function () {
        var val = $(this).val();
        $("#" + taxonomy + "checklist input:checkbox").each(function () {
            var v = $(this).val();
            var k = $(this).parent().text().trim();
            var c = this.checked;
            if (c == true && c != val) $(this).click();
            if (c == false && v == val) $(this).click();
        });
    });
    $(".inside #taxonomy-" + taxonomy).hide();
}
