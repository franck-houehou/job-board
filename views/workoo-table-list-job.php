<?php
require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');

class WorkooTableList extends WP_List_Table{
	//define data  set for WP_List_Table => data
	//prepare_items

	public function prepare_items(){

		$orderby = isset($_GET['orderby']) ? trim($_GET['orderby']) : "";
		$order = isset($_GET['order']) ? trim($_GET['order']) : "";

		$search_term = isset($_POST['s']) ? trim($_POST['s']) : "";

		$datas = $this->wp_list_table_data($orderby, $order, $search_term);

		$per_page = 3;
		$current_page = $this->get_pagenum();
		$total_items = count($datas);

		$this->set_pagination_args(array(
			"total_items" => $total_items,
			"per_page" => $per_page,
		));

		$this->items = array_slice($datas, (($current_page-1)*$per_page), $per_page);

		$columns = $this->get_columns();

		$hidden = $this->get_hidden_columns();

		$sortable = $this->get_sortable_columns();

		$this->_column_headers = array($columns, $hidden, $sortable);
	}

	public function wp_list_table_data($orderby="", $order="", $search_term=""){
		global $wpdb;

		if(!empty($search_term)){
			$all_applys = $wpdb->get_results ("SELECT * FROM  wp_applyjob WHERE last_name LIKE '%$search_term%' OR first_name LIKE '%$search_term%' OR email LIKE '%$search_term%' OR phone LIKE '%$search_term%' OR current_post_reference LIKE '%$search_term%' OR current_user_email LIKE '%$search_term%'" );
		}else{
			if($orderby=="last_name" && $order=="desc"){
				$all_applys = $wpdb->get_results ("SELECT * FROM  wp_applyjob ORDER BY last_name desc" );
			}elseif ($orderby=="last_name" && $order=="asc") {
				$all_applys = $wpdb->get_results ("SELECT * FROM  wp_applyjob ORDER BY last_name asc" );
			}else{
				$all_applys = $wpdb->get_results ("SELECT * FROM  wp_applyjob ORDER BY ID desc" );
			}
		}


		

		// if($orderby=="first_name" && $order=="desc"){
		// 	$all_applys = $wpdb->get_results ("SELECT * FROM  wp_applyjob ORDER BY first_name desc" );
		// }elseif ($orderby=="first_name" && $order=="asc") {
		// 	$all_applys = $wpdb->get_results ("SELECT * FROM  wp_applyjob ORDER BY first_name asc" );
		// }else{
		// 	$all_applys = $wpdb->get_results ("SELECT * FROM  wp_applyjob ORDER BY ID desc" );
		// }
		$uploads = wp_upload_dir();
		$upload_url = $uploads['url'];
		$applys_array = array();
		if(count($all_applys)>0){

			foreach ($all_applys as $index => $apply) {
				$applys_array[] = array(

					"id" => $apply->ID,
					"last_name" => $apply->last_name,
					"first_name" => $apply->first_name,
					"email" => $apply->email,
					"phone" => $apply->phone,
					"cv" => "<a href='".$upload_url.'/'.$apply->cv."' target='_blank'>Télécharger</a>",
					"current_post_reference" => $apply->current_post_reference,
					"current_user_email" => $apply->current_user_email,
					"date_soumission" => date('d/m/Y H:i',strtotime($apply->date_soumission)),
				);
			}
		}
		
		return $applys_array;
	}

	public function get_hidden_columns(){

		return array("id");
	}

	public function get_sortable_columns(){
		
		return array(
			"last_name" => array("last_name", true),
			// "first_name" => array("first_name", true),
		);
	}

	//get_columns

	public function get_columns(){

		$columns = array(

			"id" => "ID",
			"last_name" => "Nom",
			"first_name" => "Prénoms",
			"email" => "Email",
			"phone" => "Téléphone",
			"cv" => "CV",
			"current_post_reference" => "Offre Ref",
			"current_user_email" => "Identifiant de connexion",
			"date_soumission" => "Date",

		);
		return $columns;
	}

	//column_default

	public function column_default($item, $column_name){

		switch ($column_name) {
			case 'id':
			case 'last_name':
			case 'first_name':
			case 'email':
			case 'phone':
			case 'cv':
			case 'current_post_reference':
			case 'current_user_email':
			case 'date_soumission':
				return $item[$column_name];
			
			default:
				return "no value";
		}

	}
}

function workoo_show_data_list_table(){

	$workoo_table = new WorkooTableList(); 

	$workoo_table->prepare_items();

	echo "<h1>Liste des somissions</h1>";
	echo "<form method='post' name='form_search_apply' action='".$_SERVER['PHP_SELF']."?post_type=job&page=soumissions'>";
	$workoo_table->search_box("Rechercher","search_apply_id");
	echo "</form>";
	$workoo_table->display();
}

workoo_show_data_list_table();