<?php get_header();?>
<!-- ============================ Page Title Start================================== -->
<?php if(have_posts()) : while (have_posts()) : the_post();

	$nom_entreprise =  get_post_meta(get_the_ID(), 'workoo_nom_entreprise', true);
	$lieu_travail =  get_post_meta(get_the_ID(), 'workoo_lieu_travail', true);
	$date_limite =  get_post_meta(get_the_ID(), 'workoo_date_limite', true);
	$remuneration =  get_post_meta(get_the_ID(), 'workoo_remuneration', true);
	$lieu_travail =  get_post_meta(get_the_ID(), 'workoo_lieu_travail', true);
	$mission =  get_post_meta(get_the_ID(), 'workoo_mission', true);
	$tache =  get_post_meta(get_the_ID(), 'workoo_tache', true);
	$profil =  get_post_meta(get_the_ID(), 'workoo_profil', true);
	$competence =  get_post_meta(get_the_ID(), 'workoo_competence', true);
	$candidature =  get_post_meta(get_the_ID(), 'workoo_candidature', true);
	?>
<div class="page-title search-form dark">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				
				<div class="_jb_details01">
					
					<div class="_jb_details01_flex">
						<div class="_jb_details01_authors_caption" style="margin-left: 16px;">
							<h4 class="jbs_title"><?php the_title(); ?><img src="assets/img/verify.svg" class="ml-1" width="12" alt=""></h4>
							<ul class="jbx_info_list">
								<li><span><i class="ti-briefcase"></i><?=$nom_entreprise;?></span></li>
								<li><span><i class="ti-credit-card"></i><?=$remuneration;?></span></li>
								<li><span><i class="ti-location-pin"></i><?=$lieu_travail;?></span></li>
								<li><span><i class="ti-timer"></i><?=date('d/m/Y',strtotime($date_limite));?></span></li>
							</ul>

							<ul class="jbx_info_list">
								<li><span class="tax_title">Secteur :</span> <div class="jb_types fulltime tax_content"> <?php the_terms(get_the_ID(), 'secteur');?></div></li>

								<li><span class="tax_title">Niveau :</span> <div class="jb_types urgent tax_content"> <?php the_terms(get_the_ID(), 'niveau');?></div></li>
							</ul>
						</div>
					</div>
					
					<div class="_jb_details01_last">
						<ul class="_flex_btn">
							<li>
								<a class="_applied_jb" href="javascript:void(0);" data-toggle="modal" data-target="#upload-resume">Postuler</a>
							</li>
						</ul>
					</div>
					
				</div>
				
			</div>
		</div>
	</div>
</div>
<!-- ============================ Page Title End ================================== -->

<!-- ============================ Main Section Start ================================== -->
<section>
	<div class="container">
		<div class="row">
		
			<div class="" style="width: 80%; margin: auto;">
				<div class="_job_detail_box">
					
					<div class="_job_detail_single">
						<h4>Mission</h4>
						<p><?=$mission;?></p>
					</div>
					
					<div class="_job_detail_single">
						<h4>Tâches et activités</h4>
						<p><?=$tache;?></p>
					</div>

					<div class="_job_detail_single">
						<h4>Profil</h4>
						<p><?=$profil;?></p>
					</div>
					
					<div class="_job_detail_single">
						<h4>Compétences</h4>
						<p><?=$competence;?></p>
					</div>

					<div class="_job_detail_single">
						<h4>Candidature</h4>
						<p><?=$candidature;?></p>
					</div>
					
					<div class="_job_detail_single flexeo">
						<!-- <div class="_job_detail_single_flex">
							<ul class="shares_jobs">
								<li>Share The Job</li>
								<li><a href="#" class="share fb"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" class="share tw"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" class="share gp"><i class="fa fa-google"></i></a></li>
								<li><a href="#" class="share ln"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div> -->
						
						<div class="_exlio_buttons" style="width: 100%;">
							<ul class="bottoms_applies">
								<li style="float: right;">

									<a class="_applied_jb" href="javascript:void(0);" data-toggle="modal" data-target="#upload-resume">Postuler</a>
								</li>
							</ul>
						</div>
						
					</div>
					
				</div>
			</div>
			
		</div>
	</div>
</section>
<?php endwhile; endif; ?>

<?php get_footer();?>