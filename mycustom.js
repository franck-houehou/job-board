jQuery(document).ready(function ($) {
    $('form#apply').on('submit', function (e) {
        e.preventDefault();
        $('.indicator-apply').show();
        $('#submit_apply').prop("disabled", true);
        $('.result-message-apply').hide();

        var last_name  = $('#last_name').val();
        var first_name  = $('#first_name').val();
        var email  = $('#email_user').val();
        var phone  = $('#phone').val();
        var uploadResume= $('#uploadResume')[0].files.length;
        console.log(uploadResume);

        if(last_name!='' || first_name!='' || email!='' || phone!='' || uploadResume!=0){

            $.ajax({
                url: mycustom_vars.ajaxurl,
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    var data = JSON.parse(data);
                    if( data ) {
                        $('.indicator-apply').hide();
                        if(data.status) {
                            window. location. reload();
                            $('.result-message-apply').html( data.message ); 
                            $('.result-message-apply').removeClass('alert-danger');
                            $('.result-message-apply').addClass('alert-success');
                            $('.result-message-apply').show(); 
                        }else {
                            $('#submit_apply').prop("disabled", false);
                            $('.result-message-apply').html( data.message ); 
                            $('.result-message-apply').addClass('alert-danger');
                            $('.result-message-apply').show(); 
                        }
                    }
                }
            });   

        }else{
            $('.indicator-apply').hide();
            $('#submit_apply').prop("disabled", false);
            $('.result-message-apply').html( 'Veuillez saisir tous les champs' ); 
            $('.result-message-apply').addClass('alert-danger');
            $('.result-message-apply').show(); 
        }
    });

});