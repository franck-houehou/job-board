<?php get_header();?>

<?php 

$args = array(
	'post_type' => 'job',
    'meta_query'  => array(
    	array(
	        'key' => 'workoo_date_limite',
	        'value' => date("Y-m-d H:i:s"),
	        'compare' => '>',
	        'type' => 'DATETIME'
	    )
    )
);

$my_query = new WP_Query( $args );

?>

<!-- ============================ Hero Banner  Start================================== -->
			<div class="hero-banner full image-bottom center" style="background:#2944c1 url(<?php bloginfo('template_directory');?>/assets/img/bn-1.png) no-repeat;" data-overlay="0">
				<div class="container">
					<h1><span class="count"><?=$my_query->post_count?></span> Offres d'emplois trouvées</h1>
					<div class="mt-5">
						<div class="row m-0 justify-content-center">
							<div class="col-lg-10 col-md-10">
								<form class="search-big-form shadows">
									<div class="row m-0">
										<div class="col-lg-5 col-md-5 col-sm-12 p-0">
											<div class="form-group">
												<i class="ti-search"></i>
												<input type="text" class="form-control l-radius b-0 b-r" placeholder="Job Title or Keywords">
											</div>
										</div>
										
										<div class="col-lg-5 col-md-4 col-sm-12 p-0">
											<div class="form-group">
												<i class="ti-location-pin"></i>
												<input type="text" class="form-control b-0" placeholder="San Francisco, CA">
											</div>
										</div>
										
										<div class="col-lg-2 col-md-3 col-sm-12 p-0">
											<button type="button" class="btn theme-bg r-radius full-width">Rechercher</button>
										</div>
									</div>
								</form>	
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- ============================ Hero Banner End ================================== -->
			
			<!-- ============================ Newest Jobs Start ==================================== -->
			<section class="light-w min-sec">
				<div class="container">
				
					<div class="row justify-content-center">
						<div class="col-lg-7 col-md-9">
							<div class="sec-heading">
								<h2>Nos offres d'<span class="theme-cl-2">emplois</span></h2>
							</div>
						</div>
					</div>
					<?php get_template_part( 'job-front' ); ?>	
				</div>
			</section>
			<?php if (is_user_logged_in()) { get_template_part( 'user-connected-jobs' );}?>
			<!-- ============================ Call To Action End ================================== -->
<?php get_footer();?>