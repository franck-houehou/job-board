<!-- Log In Modal -->
			<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="registermodal" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered login-pop-form" role="document">
					<div class="modal-content" id="registermodal">
						<div class="modal-header">
							<h4 class="show_title"></h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ti-close"></i></span></button>
						</div>
						<div class="modal-body">
							
							<div class="login-form">
								<div class="alert alert-warning indicator" style="display: none">Veuillez patienter...</div>
    							<div class="alert result-message" style="display: none"></div>
    							
								<form id="register">
									<?php wp_nonce_field('vb_new_user','vb_new_user_nonce', true, true ); ?>
								
									<div class="form-group">
										<label>E mail</label>
										<input type="email" id="email" name="email" class="form-control" placeholder="Email" required="">
									</div>
									
									<div class="form-group">
										<label>Mot de passe</label>
										<input type="password" id="password" name="password" class="form-control" placeholder="*******" required="">
									</div>
									
									<div class="form-group">
										<button type="submit" id="submit_register" class="btn dark-2 btn-md full-width pop-login">S'incrire</button>
									</div>
								</form>

								<form id="login">
									<?php wp_nonce_field('vb_user','vb_user_nonce', true, true ); ?>
								
									<div class="form-group">
										<label>E mail</label>
										<input type="email" id="email_login" name="email_login" class="form-control" placeholder="Email" required="">
									</div>
									
									<div class="form-group">
										<label>Mot de passe</label>
										<input type="password" id="password_login" name="password_login" class="form-control" placeholder="*******" required="">
									</div>
									
									<div class="form-group">
										<button type="submit" id="submit_login" class="btn dark-2 btn-md full-width pop-login">Se connecter</button>
									</div>
								</form>
							</div>

							
						</div>
						<div class="modal-footer">
							<div class="mf-link" style="display: none;" id="show_login_footer"><a href="" id="show_login" class="theme-cl"> Connectez vous</a></div>

							<div class="mf-link" style="display: none;" id="show_register_footer"><a href="" id="show_signup" class="theme-cl"> Inscrivez vous</a></div>
							
							<div class="mf-forget"><a href="<?php echo wp_lostpassword_url(); ?>">Mot de passe oublié</a></div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Modal -->