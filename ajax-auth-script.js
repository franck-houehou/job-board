jQuery(document).ready(function ($) {

    $('form#register').on('submit', function (e) {
        // if (!$(this).valid()) return false;
        console.log(ajax_auth_object.loadingmessage);
        $('.status').html(ajax_auth_object.loadingmessage);
        action = 'ajaxlogin';
        email =  $('form#login #email').val();
        password = $('form#login #password').val();
        security = $('form#login #security').val();
        if ($(this).attr('id') == 'register') {
            action = 'ajaxregister';
            email =  $('form#register #email').val();
            password = $('form#register #password').val();
            security = $('#signonsecurity').val(); 
        }  
        ctrl = $(this);
        $.ajax({
            type: 'POST',
            url: ajax_auth_object.ajaxurl,
            data: {
                'action': action,
                'password': password,
                'email': email,
                'security': security
            },
            // success: function (data) {
            //     $('p .status').html(data.message);
            //     if (data.loggedin == true) {
            //         document.location.href = ajax_auth_object.redirecturl;
            //     }
            // }
        }).done(function (data) {
                        $('.status').html(data.message);
                        if (data.loggedin == true) {
                            document.location.href = ajax_auth_object.redirecturl;
                        }
                    });
        e.preventDefault();
    });
    

    // Close popup
    $(document).on('click', '.login_overlay, .close', function () {
        $('form#login, form#register').fadeOut(500, function () {
            $('.login_overlay').remove();
        });
        return false;
    });

    // Show the login/signup popup on click
    // $('#show_login, #show_signup').on('click', function (e) {
    //     alert('hihi');
    //     $('body').prepend('<div class="login_overlay"></div>');
    //     if ($(this).attr('id') == 'show_login') 
    //         $('form#login').fadeIn(500);
    //     else 
    //         $('form#register').fadeIn(500);
    //     e.preventDefault();
    // });



    // Perform AJAX login/register on form submit
    
    
    // Client side form validation
   // if (jQuery("#register").length) 
   //      jQuery("#register").validate(
   //      { 
   //          rules:{
   //          password2:{ equalTo:'#signonpassword' 
   //          }   
   //      }}
   //      );
   //  else if (jQuery("#login").length) 
   //      jQuery("#login").validate();

});

function make_dropdown($, taxonomy) {
    var opts = '<option value="0">- n/a -</option>';
    $(
        "#taxonomy-" + taxonomy + " #" + taxonomy + "checklist label.selectit"
    ).each(function () {
        var v = $(this).find("input:checkbox").val();
        var k = $(this).text().trim();
        var c = $(this).find("input:checkbox").get(0).checked
            ? ' selected="selected" '
            : "";
        opts += '<option value="' + v + '" ' + c + ">" + k + "</option>";
    });
    $(
        '<div id="taxonomy-' +
        taxonomy +
        '-dropdown-holder"><select id="taxonomy-' +
        taxonomy +
        '-dropdown" style="width:100%;">' +
        opts +
        "</select></div>"
    ).insertAfter("#taxonomy-" + taxonomy);

    $("#taxonomy-" + taxonomy + "-dropdown").change(function () {
        var val = $(this).val();
        $("#" + taxonomy + "checklist input:checkbox").each(function () {
            var v = $(this).val();
            var k = $(this).parent().text().trim();
            var c = this.checked;
            if (c == true && c != val) $(this).click();
            if (c == false && v == val) $(this).click();
        });
    });
    $("#taxonomy-" + taxonomy).hide();
}



// jQuery(document).ready(function ($) {});
