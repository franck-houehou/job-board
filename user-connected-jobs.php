<?php 
require_once(ABSPATH . 'wp-content/themes/workoo/functions/format-date.php');

global $wpdb;
$current_user_id= wp_get_current_user()->ID;

$jobs = $wpdb->get_results ("SELECT posts.*, applyjob.* FROM  wp_posts as posts, wp_applyjob as applyjob WHERE posts.ID=applyjob.current_post_id AND applyjob.current_user_id=$current_user_id" );
?>




<section class="min-sec">
				<div class="container">
				
					<div class="row justify-content-center">
						<div class="col-lg-7 col-md-9">
							<div class="sec-heading">
								<h2>Offres auxquelles vous avez <span class="theme-cl-2">postulées</span></h2>
							</div>
						</div>
					</div>
					
					<div class="row justify-content-center">

					<?php if ($jobs): ?>

					<?php global $post; ?>
					<?php foreach ($jobs as $post): ?>
					  <?php setup_postdata($post); ?>
					  <?php
						$nom_entreprise =  get_post_meta($post->current_post_id, 'workoo_nom_entreprise', true);
						$lieu_travail =  get_post_meta($post->current_post_id, 'workoo_lieu_travail', true);
						$date_limite =  get_post_meta($post->current_post_id, 'workoo_date_limite', true);
						$remuneration =  get_post_meta($post->current_post_id, 'workoo_remuneration', true);
						$lieu_travail =  get_post_meta($post->current_post_id, 'workoo_lieu_travail', true);
					  ?>

					  <!-- Single Jobs -->
						<div class="col-lg-4 col-md-6 col-sm-12">
							<div class="_jb_list72">
								<div class="jobs-like bookmark">
									<label class="toggler toggler-danger"><input type="checkbox"><i class="fa fa-bookmark"></i></label>
								</div>
								<div class="_jb_list72_flex">
									<div class="_jb_list72_last">
										<h4 class="_jb_title"><a href="job-detail.html"><?php the_title(); ?></a></h4>
										<div class="_times_jb"><?=$remuneration;?>/mois</div>
										<div class="_times_jb"><?=$nom_entreprise;?>, <?=$lieu_travail;?></div>
									</div>
								</div>
								<div class="_jb_list72_foot">
									<div class="_times_jb"><?=time_elapsed_string($post->date_soumission);?></div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>

					<?php else : ?>
						<!-- <div class="row justify-content-center"> -->
							<p>Vous n'avez pas encore postulés à une offre</p>
						<!-- </div> -->
					<?php endif; wp_reset_postdata();?>
						
					</div>
					
				</div>
			</section>