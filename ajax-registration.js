jQuery(document).ready(function ($) {

    $('form#register').on('submit', function (e) {
        // if (!$(this).valid()) return false;
        $('.indicator').show();
        $('#submit_register').prop("disabled", true);
        $('.result-message').hide();

        var reg_nonce = $('#vb_new_user_nonce').val();
        var reg_user  = $('#email').val();
        var reg_pass  = $('#password').val();
        var reg_mail  = $('#email').val();
        var action = 'register_user';

        var ajax_url = vb_reg_vars.vb_ajax_url;
        // Data to send
        data = {
          action: action,
          nonce: reg_nonce,
          user: reg_user,
          pass: reg_pass,
          mail: reg_mail
        };

        $.post( ajax_url, data, function(response) {
            // If we have response
            var data = JSON.parse(response);
            if( data ) {

            // Hide 'Please wait' indicator
            $('.indicator').hide();
            if(data.status) {
              // If user is created
              $('.result-message').html(data.message); // Add success message to results div
              $('.result-message').removeClass('alert-danger');
              $('.result-message').addClass('alert-success'); // Add class success to results div
              $('.result-message').show(); // Show results div
            } else {
                $('#submit_register').prop("disabled", false);
                $('.result-message').html(data.message); // If there was an error, display it in results div
                $('.result-message').addClass('alert-danger'); // Add class failed to results div
                $('.result-message').show(); // Show results div
            }
            }
            });
            e.preventDefault();
    });




    $('form#login').on('submit', function (e) {
        // if (!$(this).valid()) return false;
        $('.indicator').show();
        $('#submit_login').prop("disabled", true);
        $('.result-message').hide();

        var reg_nonce = $('#vb_user_nonce').val();
        var reg_user  = $('#email_login').val();
        var reg_pass  = $('#password_login').val();
        var reg_mail  = $('#email_login').val();
        var action = 'login_user';
        var ajax_url = vb_reg_vars.vb_ajax_url;
        data = {
          action: action,
          nonce: reg_nonce,
          user: reg_user,
          pass: reg_pass,
          mail: reg_mail
        };
        $.post( ajax_url, data, function(response) {
          // If we have response
          var data = JSON.parse(response);
          if( data ) {
            // Hide 'Please wait' indicator
            $('.indicator').hide();
            if(data.status) {
                document.location.href = vb_reg_vars.redirecturl;
                $('.result-message').html( data.message ); 
                $('.result-message').removeClass('alert-danger');
                $('.result-message').addClass('alert-success');
                $('.result-message').show(); 
            } else {
                $('#submit_login').prop("disabled", false);
                $('.result-message').html( data.message ); 
                $('.result-message').addClass('alert-danger');
                $('.result-message').show(); 
            }
          }
        });
        e.preventDefault();
    });


    $('#show_login, #show_signup').on('click', function (e) {
        $('.result-message').hide();
        $('.indicator').hide();
        $('form#login').fadeOut(500);
        $('form#register').fadeOut(500);
        if ($(this).attr('id') == 'show_login') {

            $('form#login').fadeIn(500);
            $('.show_title').html('Connexion');
            $('#show_login_footer').hide();
            $('#show_register_footer').show();

        }else {

            $('form#register').fadeIn(500);
            $('.show_title').html('Inscription');
            $('#show_register_footer').hide();
            $('#show_login_footer').show();
        }
            
        e.preventDefault();
    });

});