<!-- =========================== Footer Start ========================================= -->
			<footer class="dark-footer skin-dark-footer">
				<div>
					<div class="container">
						<div class="row">
							
							<div class="col-lg-4 col-md-6">
								<div class="footer-widget">
									<img src="<?php bloginfo('template_directory');?>/assets/img/logo-light.png" class="img-fluid f-logo" width="120" alt="">
									<p>407-472 Rue Saint-Sulpice, Montreal<br>Quebec, H2Y 2V8</p>
									<ul class="footer-bottom-social">
										<li><a href="#"><i class="ti-facebook"></i></a></li>
										<li><a href="#"><i class="ti-twitter"></i></a></li>
										<li><a href="#"><i class="ti-instagram"></i></a></li>
										<li><a href="#"><i class="ti-linkedin"></i></a></li>
									</ul>
								</div>
							</div>		
							<div class="col-lg-2 col-md-4">
								<div class="footer-widget">
									<h4 class="widget-title">Useful links</h4>
									<ul class="footer-menu">
										<li><a href="#">About Us</a></li>
										<li><a href="#">FAQs Page</a></li>
										<li><a href="#">Checkout</a></li>
										<li><a href="#">Login</a></li>
									</ul>
								</div>
							</div>
							
							<div class="col-lg-2 col-md-4">
								<div class="footer-widget">
									<h4 class="widget-title">Developers</h4>
									<ul class="footer-menu">
										<li><a href="#">Booking</a></li>
										<li><a href="#">Stays</a></li>
										<li><a href="#">Adventures</a></li>
										<li><a href="#">Author Detail</a></li>
									</ul>
								</div>
							</div>
							
							<div class="col-lg-2 col-md-4">
								<div class="footer-widget">
									<h4 class="widget-title">Useful links</h4>
									<ul class="footer-menu">
										<li><a href="#">About Us</a></li>
										<li><a href="#">Jobs</a></li>
										<li><a href="#">Events</a></li>
										<li><a href="#">Press</a></li>
									</ul>
								</div>
							</div>
									
							<div class="col-lg-2 col-md-4">
								<div class="footer-widget">
									<h4 class="widget-title">Useful links</h4>
									<ul class="footer-menu">
										<li><a href="#">Support</a></li>
										<li><a href="#">Contact Us</a></li>
										<li><a href="#">Privacy &amp; Terms</a></li>
									</ul>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				
				<div class="footer-bottom">
					<div class="container">
						<div class="row align-items-center">
							
							<div class="col-lg-12 col-md-12 text-center">
								<p class="mb-0">© 2021 Workoo. Designd By <a href="https://themezhub.com/">Themez Hub</a> All Rights Reserved</p>
							</div>
							
						</div>
					</div>
				</div>
			</footer>
			<!-- =========================== Footer End ========================================= -->
			
			<?php get_template_part( 'signup' ); ?>

			<!-- Upload Resume -->
			<div class="modal fade" id="upload-resume" tabindex="-1" role="dialog" aria-labelledby="resumeupload" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered login-pop-form" role="document">
					<div class="modal-content" id="resumeupload">
						<div class="modal-header">
							<h4>Postuler à l'offre</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ti-close"></i></span></button>
						</div>
						<div class="modal-body">
							
							<div class="login-form">
								<form id="apply">
									<?php wp_nonce_field('apply','apply_nonce', true, true ); ?>
									<div class="form-row">
										<div class="col-lg-6 col-md-12">
											<div class="form-group">
												<label>Nom *</label>
												<input type="text" id="last_name" name="last_name" class="form-control" placeholder="Nom" required>
											</div>
										</div>
										
										<div class="col-lg-6 col-md-12">
											<div class="form-group">
												<label>Prénoms *</label>
												<input type="text" id="first_name" name="first_name" class="form-control" placeholder="Prénoms" required>
											</div>
										</div>
									</div>

									<div class="form-row">
										<div class="col-lg-6 col-md-12">
											<div class="form-group">
												<label>Email *</label>
												<input type="email" id="email_user" name="email_user" class="form-control" placeholder="Email" required>
											</div>
										</div>
										<div class="col-lg-6 col-md-12">
											<div class="form-group">
												<label>Téléphone *</label>
												<input type="text" id="phone" name="phone" class="form-control" placeholder="Téléphone" required>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label>Lettre de motivation</label>
										<textarea class="form-control ht-150" id="motivation" name="motivation" placeholder="Lettre de motivation"></textarea>
									</div>
									
									<div class="form-group">
										<label class="light">doc, docx,pdf,txt,png *</label>
										<div class="custom-file">
											<input type="file" name="uploadResume" class="custom-file-input" id="uploadResume" required>
											<label class="custom-file-label" for="uploadResume"><i class="ti-link mr-1"></i>Téléverser votre cv</label>
										</div>
									</div>

									<div class="alert alert-warning indicator-apply" style="display: none">Veuillez patienter...</div>
    								<div class="alert result-message-apply" style="display: none"></div>
									
									<div class="form-group">
										<button type="submit" id="submit_apply" class="btn dark-2 btn-md full-width pop-login">Soumettre</button>
										<input type="hidden" name="action" value="apply_job">

										<input type="hidden" name="current_post_id" id="current_post_id" value="<?=get_the_ID()?>" name="">

										<input type="hidden" name="current_user_id" id="current_user_id" value="<?=wp_get_current_user()->ID?>" name="">
										
										<input type="hidden" name="current_user_email" id="current_user_email" value="<?=wp_get_current_user()->user_email?>">

										<input type="hidden" name="current_post_reference" id="current_post_reference" value="<?=get_post_meta(get_the_ID(), 'workoo_reference', true)?>">

										<input type="hidden" name="current_post_title" id="current_post_title" value="<?=the_title()?>">

									</div>
								
								</form>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<!-- Upload Resume -->		

		</div>
		<?php wp_footer(); ?>
		
	</body>

<!-- Mirrored from themezhub.net/workoo-demo/workoo/home-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Apr 2021 09:08:09 GMT -->
</html>