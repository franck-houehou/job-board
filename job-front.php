<?php 

$args = array(
    'post_type' => 'job',
    'posts_per_page' => 6,
    'meta_query'  => array(
    array(
        'key' => 'workoo_date_limite',
        'value' => date("Y-m-d H:i:s"),
        'compare' => '>',
        'type' => 'DATETIME'
    )
)
);?>
<!-- <div class="row justify-content-center"> -->

<?php $my_query = new WP_Query( $args );

if($my_query->have_posts()): ?>
	<div class="row justify-content-center">
	
	<?php while ($my_query->have_posts()) : $my_query->the_post();  

		$nom_entreprise =  get_post_meta(get_the_ID(), 'workoo_nom_entreprise', true);
		$lieu_travail =  get_post_meta(get_the_ID(), 'workoo_lieu_travail', true);
		$date_limite =  get_post_meta(get_the_ID(), 'workoo_date_limite', true);
		$remuneration =  get_post_meta(get_the_ID(), 'workoo_remuneration', true);
		$lieu_travail =  get_post_meta(get_the_ID(), 'workoo_lieu_travail', true);
		?>
		

	<div class="col-lg-4 col-md-6 col-sm-6">
		<div class="_jb_list73">
			<div class="_jb_list73_header">
				<div class="jobs-like bookmark">
					<label class="toggler toggler-danger"><input type="checkbox"><i class="fa fa-bookmark"></i></label>
				</div>
				<div class="_jb_list72_flex">
					
					<div class="_jb_list72_last">
						<h4 class="_jb_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<div class="_times_jb"><?=$nom_entreprise;?>, <?=$lieu_travail;?></div>
					</div>
				</div>
				<div class="_jb_list72_foot">
					<div class="_times_jb text-right"><?=date('d/m/Y',strtotime($date_limite));?></div>
				</div>
			</div>
			<div class="_jb_list73_middle">
				<div class="_jb_list73_middle_flex">
					<h4 class="_jb_title"><a href="<?php the_permalink(); ?>">Rémunération</a></h4>
					<div class="_times_jb"><?=$remuneration;?>/mois</div>
				</div>
				<div class="applieded_list">
					<div class="_jb_types fulltime_lite"><a href="<?php the_permalink(); ?>">Voir détails</a></div>
				</div>
			</div>
		</div>	
	</div>
<?php endwhile ?>
</div>

<div class="row justify-content-center">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="mt-3 text-center">
			<a href="<?=home_url();?>/job" class="_browse_more-2 light">Voir plus d'offres</a>
		</div>
	</div>
</div>
<?php else : ?>
	<div class="row justify-content-center">
		<p>Aucune offre n'est disponible actuellement</p>
	</div>
<?php endif; wp_reset_postdata();?>

