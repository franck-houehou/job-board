<!DOCTYPE html>
<html lang="zxx">
	
<!-- Mirrored from themezhub.net/workoo-demo/workoo/home-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Apr 2021 09:08:07 GMT -->
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
        <title>Workoo - Freelancing HTML Template</title>
		<?php wp_head(); ?>
        
    </head>
	
    <body class="blue-skin">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="Loader"></div>
		
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
		
            <!-- ============================================================== -->
            <!-- Top header  -->
            <!-- ============================================================== -->
            <!-- Start Navigation -->
			<div class="header header-transparent change-logo">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<nav id="navigation" class="navigation navigation-landscape">
								<div class="nav-header">
									<a class="nav-brand static-logo" href="<?=home_url()?>"><img src="<?php bloginfo('template_directory');?>/assets/img/logo-light.png" class="logo" alt="" /></a>
									<a class="nav-brand fixed-logo" href="#"><img src="<?php bloginfo('template_directory');?>/assets/img/logo.png" class="logo" alt="" /></a>
									<div class="nav-toggle"></div>
								</div>
								<div class="nav-menus-wrapper">
									<ul class="nav-menu">
									
										<li><a href="<?=home_url()?>">Accueil</a></li>
										
										<li><a href="<?=home_url();?>/job">Nos offres</a></li>

										<?php if (is_user_logged_in()) { ?>

										<li><a href="#">Vos soumissions</a></li>
										<?php } ?>
										
									</ul>
									
									<ul class="nav-menu nav-menu-social align-to-right">

										<?php if (is_user_logged_in()) { ?>
											<li class="add-listing bg-whit" style="background: #df3411!important; color: white!important;">
												<a style="color: white!important;" href="<?php echo wp_logout_url( home_url() ); ?>"> Se déconnecter
												</a>
											</li>
										<?php } else { ?>            	
										        <li>
													<a href="#" data-toggle="modal" data-target="#login" id="show_login"> Se connecter
													</a>
												</li>

												<li>
													<a href="#" data-toggle="modal" data-target="#login" id="show_signup"> S'inscrire
													</a>
												</li>
										<?php } ?>

										
									</ul>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
			<!-- End Navigation -->
			<div class="clearfix"></div>
			<!-- ============================================================== -->
			<!-- Top header  -->
			<!-- ============================================================== -->